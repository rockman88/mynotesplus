package com.rockman88.mynotesplus.adaptor;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.rockman88.mynotesplus.R;
import com.rockman88.mynotesplus.db.DBOpenHelper;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class NotesCursorAdapter extends CursorAdapter{
    public NotesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(
                R.layout.note_list_item, parent, false
        );
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // TODO: Find out why we have to add new column name to DBOpenHelper.ALL_COLUMN for this to
        // TODO:  know about the new column
        String noteTitle = cursor.getString(
                cursor.getColumnIndex(DBOpenHelper.NOTE_TITLE));

        // Clean content
        if (noteTitle != null) {
            if (noteTitle.indexOf("<br>") >= 0) {
                noteTitle = noteTitle.substring(0, noteTitle.indexOf("<br>"));
            }

            noteTitle = htmlClean(noteTitle);

            int pos = noteTitle.indexOf(10);
            if (pos != -1) {
                noteTitle = noteTitle.substring(0, pos) + " ...";
            }

            if (noteTitle.trim().equals("")) noteTitle = "No Title";
        } else {
            noteTitle = "No Title";
        }

        TextView tv = view.findViewById(R.id.tvNote);

        tv.setText(noteTitle);

    }

    private String htmlClean(String s) {
        return Jsoup.clean(s, new Whitelist());
    }
}
