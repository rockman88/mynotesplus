package com.rockman88.mynotesplus.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper{

    //Constants for db name and version
    private static final String DATABASE_NAME = "mynotesplus.db";
    private static final int DATABASE_VERSION = 1;

    //Constants for identifying table and columns
    public static final String TABLE_NOTES = "notes";
    public static final String NOTE_ID = "_id";
    public static final String NOTE_TITLE = "noteTitle";
    public static final String NOTE_TEXT = "noteText";
    public static final String NOTE_CREATED = "noteCreated";

    public static final String[] ALL_COLUMNS =
            {"_id", "noteText", "noteCreated", "noteTitle"};

    //SQL to create table
    private static final String TABLE_CREATE =
            String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT default CURRENT_TIMESTAMP)",
                    TABLE_NOTES, NOTE_ID, NOTE_TEXT, NOTE_TITLE, NOTE_CREATED);

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: This gets called when an app upgrade changes the database version number
        // TODO: We want to alter the database if possible to preserve the user's data
        // TODO: Reference https://stackoverflow.com/questions/21881992/when-is-sqliteopenhelper-oncreate-onupgrade-run

        // This is dev code.  It just delete table if a new db version is detected.
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
        //onCreate(db);
        // END

        //db.execSQL("ALTER TABLE " + TABLE_NOTES + " ADD COLUMN " + NOTE_TITLE + " TEXT");
    }
}
